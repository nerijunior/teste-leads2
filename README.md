# Teste Leads2

## Exercício

No email que recebi estava escrito assim:

>Exercício:
>Desenvolva uma WebApp que use o vue -> https://vuejs.org
>
>Esta App deve consumir dados de uma api que deve ser escrita em PHP, retornando os dados dos funcionários de uma empresa (O banco de dados deve ser MySQL).

Como eu devo entregar esse exercício até as 17:00 não queria perder tempo fazendo um monte de coisas na mão, resolvi usar o vue-cli com o template do webpack para agilizar a parte do client e Laravel para a API, espero que isso não impacte, mas caso precisem ter certeza que eu consigo criar tudo isso "na mão", por mim, sem problemas.

## API

Antes de ler, prepare o ambiente:

```language-sh
cd teste-leads2/api
composer install
```

Por padrão o Laravel utiliza o arquivo `.env` para obter as configurações de ambiente, então para popular o DB e conseguirem testar a aplicação, basta copiar o `.env.example` para `.env` e alterar o que é necessário.

Antes de efetuar a migração do DB, não esqueça de criar o DB no mysql. ;)

Após as alterações rodar `php artisan migrate --seed` para criar as tabelas no DB junto com alguns dados de demonstração.

Você pode rodar a API diretamente executando `php artisan serve` o processo vai rodar em [http://localhost:8000](http://localhost:8000) essa é a configuração padrão que uso no *client*, caso estejam rodando com `valet`, `homestead` ou em uma maquina virtual, será necessário informar ao client em qual endereço está a API.

## Client

Antes de ler, prepare o ambiente:

```language-sh
cd teste-leads2/client
npm install
```

Bom, eu criei o client utilizando o template webpack usando o [vue-cli](https://github.com/vuejs/vue-cli), então não tem muito segredo, todo o conteúdo está em `/src`, para rodar em `development` basta executar:

```language-sh
npm install
npm run dev
```

Para executar em `production` basta executar:

```language-sh
npm run build
```

E colocar o diretório `/dist/` dentro do servidor web.

Caso queira integrar o client com a API, basta copiar o conteúdo:

```language-sh
cd api
cp -R [caminho até o teste-leads2]/client/dist/* ./public/
```

Após linkar é só acessar http://localhost:8000/ ou o endereço que foi configurado como explicado na sessão da API.


Qualquer dúvida pode me enviar um email no [neri@nerijunior.com](mailto:neri@nerijunior.com).
