import Api from '@/services/Api'

export default {
  logged () {
    const token = window.localStorage.getItem('token')

    if (token) {
      Api.defaults.headers.common['Authorization'] = 'Bearer ' + token
    }

    return (token)
  },
  async login (credentials) {
    try {
      const response = await Api.post('/auth/', credentials)

      if (response.data.token) {
        const token = response.data.token
        const user = response.data.user

        window.localStorage.setItem('token', token)
        window.localStorage.setItem('user', JSON.stringify(user))
        return true
      } else {
        return false
      }
    } catch (e) {
      console.log(e)
      return false
    }
  },
  logout () {
    window.localStorage.removeItem('token')
    window.localStorage.removeItem('user')
  }
}
