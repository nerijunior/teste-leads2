import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    user: {}
  },
  getters: {
    user (state) {
      return state.user
    }
  },
  mutations: {
    LOADING (state, value) {
      state.loading = value
    },
    SET_USER (state, user) {
      state.user = user
    }
  }
})
