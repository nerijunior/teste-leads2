import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import MainLayout from '@/components/MainLayout'
import EmployersList from '@/components/Employers/EmployersList'
import Auth from '@/services/Auth'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'MainLayout',
      component: MainLayout,
      meta: {
        private: true
      },
      children: [
        {
          path: '/employers',
          name: 'EmployersList',
          component: EmployersList
        }
      ]
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ]
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.private)) {
    if (!Auth.logged()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
