<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employer extends Model
{
    protected $fillable = [
        'name', 'email', 'rg', 'cpf', 'cargo', 'salario',
    ];

    public $casts = [
        'meta' => 'array',
    ];
}
