<?php

namespace App\Http\Controllers;

class ClientController extends Controller
{
    /**
     * Exibe o index criado no client
     *
     * @return Stream
     */
    public function index()
    {
        $clientIndex = public_path('index.html');

        if (file_exists($clientIndex)) {
            return file_get_contents($clientIndex);
        }

        abort(404);
    }
}
