<?php

namespace App\Http\Controllers\Api;

use App\Models\Employer;
use Illuminate\Http\Request;

class EmployerController extends Controller
{
    /**
     * Returns all employers
     * @param  Request $request
     * @return JsonRespose
     */
    public function all(Request $request)
    {
        $employers = Employer::all();

        return compact('employers');
    }
}
