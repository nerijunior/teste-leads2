<?php

namespace App\Http\Controllers\Api;

use Auth;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /**
     * Verificar se o usuário pode logar
     *
     * @return JsonResponse
     */
    public function login(Request $request)
    {
        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:6',
        ]);

        $credentials = $request->only(['email', 'password']);

        if (Auth::attempt($credentials)) {
            $token = Auth::issue();
            $user  = Auth::user();

            return compact('token', 'user');
        }

        return ['Invalid Credentials'];
    }
}
