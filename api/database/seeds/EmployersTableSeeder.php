<?php

use App\Models\Employer;
use Illuminate\Database\Seeder;

class EmployersTableSeeder extends Seeder
{
    public function run()
    {
        factory(Employer::class, 1000)->create();
    }
}
