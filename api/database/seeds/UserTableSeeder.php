<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    public function run()
    {
        $u = new User([
            'name'     => 'John Doe',
            'email'    => 'john@example.com',
            'password' => Hash::make('123qwe'),
        ]);

        $u->save();
    }
}
