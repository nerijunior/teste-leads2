<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Employer::class, function (Faker $faker) {
    $cargos = ['CEO', 'CTO', 'CIO', 'Gerente', 'Dev', 'Scrum Master'];

    return [
        'name'    => $faker->name,
        'email'   => $faker->email,
        'rg'      => rand(11111111, 999999999),
        'cpf'     => rand(11111111111, 99999999999),
        'cargo'   => $cargos[array_rand($cargos)],
        'salario' => rand(11111, 99999),
    ];
});
