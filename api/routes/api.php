<?php

use Illuminate\Http\Request;

Route::post('/auth', 'AuthController@login');

Route::middleware('auth:api')->group(function () {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::get('employers', 'EmployerController@all');

});
